var records = [
  { id: 1, username: 'taz', password: 'placeholder' }
];

exports.findByUsername = (username, cb) =>
  process.nextTick(() => cb(null, records.find(record => record.username === username)));