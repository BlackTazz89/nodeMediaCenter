const os = require('os');
const path = require('path');
const url = require('url');
const express = require('express');
const serveIndex = require('serve-index');
const passport = require('passport');
const Strategy = require('passport-http').BasicStrategy;
const logger = require('morgan');
const ffmpeg = require('fluent-ffmpeg');
const db = require('./db');

//Security config
passport.use(new Strategy(
    function (username, password, cb) {
        db.users.findByUsername(username, function (err, user) {
            if (err) { return cb(err); }
            if (!user) { return cb(null, false); }
            if (user.password != password) { return cb(null, false); }
            return cb(null, user);
        });
    }));

const video_directory = 'E:\\Backup\\Franz\\Video\\Film';
const supported_video_types = ['flv', 'avi', 'mp4', 'mkv'].join('|');
const context_path = 'myServer';

//configure expressjs
const app = express();
app.use(logger('logger'));
app.set('view engine', 'ejs');

app.use('/*', passport.authenticate('basic', { session: false }));

//Serving static directory index
app.use(`/${context_path}`, serveIndex(video_directory, { 'icons': true }));

//Redirect to videoPlayer ejs view
app.get(new RegExp(`^\/${context_path}.*\.(${supported_video_types})$`), function (req, res) {
    const req_path = req.path;
    const path_without_context = req.path.replace(`${context_path}/`, '');
    const file_ext = req_path.slice((req_path.lastIndexOf(".") - 1 >>> 0) + 2)
    res.render('videoPlayer', {
        videoSource: url.resolve(req.hostname, path_without_context),
        videoType: `video/${file_ext}`
    })
});

//Chunk video
app.get(new RegExp(`^.*(${supported_video_types})$`), function (req, res) {
    const req_path = req.path;
    const videoPath = path.join(video_directory, decodeURI(req.path.replace(`${context_path}/`, '')))
    const range = req.headers.range
    const head = {
        'Transfer-Encoding': 'chunked',
        'Content-Type': `video/mp4`
    }
    res.writeHead(200, head);
    if (range) {
        ffmpeg(videoPath)
            .on('error', function (err, stdout, stderr) {
                console.log('Cannot process video: ' + err.message);
            })
            .outputOption('-s 1280x720')
            .outputOption('-acodec aac')
            .outputOption('-vcodec h264')
            .outputOption('-movflags frag_keyframe+faststart')
            .outputOption('-f mp4')
            .output(res, { end: true })
            .run();
    } else {
        res.end();
    }
});

app.use(express.static(video_directory));

local_address = os.networkInterfaces()['Wi-Fi'].find(i => i.family === 'IPv4').address;
app.listen(3000, local_address);


//old code but useful one day
//Chunk video
// app.get(new RegExp(`^.*(${supported_video_types})$`), function (req, res) {
//     const req_path = req.path;
//     const videoPath = path.join(video_directory, req.path.replace(`${context_path}/`, ''))
//     let file_ext = req_path.slice((req_path.lastIndexOf(".") - 1 >>> 0) + 2)
//     const stat = fs.statSync(videoPath)
//     const fileSize = stat.size
//     const range = req.headers.range
//     //temp override
//     file_ext = 'mp4';
//     if (range) {
//         const parts = range.replace(/bytes=/, "").split("-")
//         const start = parseInt(parts[0], 10)
//         const end = parts[1]
//             ? parseInt(parts[1], 10)
//             : fileSize - 1
//         const chunksize = (end - start) + 1
//         const head = {
//             'Content-Range': `bytes ${start}-${end}/${fileSize}`,
//             'Accept-Ranges': 'bytes',
//             'Content-Length': chunksize,
//             'Content-Type': `video/${file_ext}`
//         }
//         res.writeHead(206, head);
//         ffmpeg(fs.createReadStream(videoPath, { start, end }))
//             .addOptions(['-crf 22', '-c:v libx264', '-f mp4', '-movflags', 'faststart+frag_keyframe'])
//             .writeToStream(res, function (retcode, error) {
//                 if (!error) {
//                     console.log('file has been converted succesfully', retcode.green);
//                 } else {
//                     console.log('file conversion error', error.red);
//                 }
//             });
//     } else {
//         const head = {
//             'Content-Length': fileSize,
//             'Content-Type': `video/${file_ext}`
//         }
//         res.writeHead(200, head)
//         ffmpeg(fs.createReadStream(videoPath))
//             .addOptions(['-crf 22', '-c:v libx264', '-f mp4', '-movflags', 'faststart+frag_keyframe'])
//             .writeToStream(res, function (retcode, error) {
//                 if (!error) {
//                     console.log('file has been converted succesfully', retcode.green);
//                 } else {
//                     console.log('file conversion error', error.red);
//                 }
//             });
//     }
// });